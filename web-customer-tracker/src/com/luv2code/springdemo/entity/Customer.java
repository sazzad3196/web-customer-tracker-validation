package com.luv2code.springdemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@Entity
@RequestMapping("/customer")
@Table(name="customer")
public class Customer {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@NotNull(message ="is required")
	@Size(min = 1, message ="is required")
	@Size(max = 50, message = "Must be first name less than or equals 50 characters")
	@Column(name="first_name")
	private String firstName;
	
	@NotNull(message ="is required")
	@Size(min = 1, message ="is required")
	@Size(max = 50, message = "Must be last name less than or equals 50 characters")
	@Column(name="last_name")
	private String lastName;
	
	@NotNull(message ="is required")
	@Size(min = 1, message ="is required")
	@Size(max = 50, message = "Must be email less than or equals 50 characters")
	@Column(name="email")
	private String email;
	
	@NotNull(message ="is required")
	@Size(min = 1, message ="is required")
	@Column(name="image")
	private String image;
	
	@NotNull(message ="is required")
	@Pattern(regexp="^[0-9]{11}", message ="only 11 digits")
	@Column(name="phone_no")
	private String phoneNo;
	
	@NotNull(message ="is required")
	@Column(name="gender")
	private String gender;
	
	@NotNull(message ="is required")
	@Column(name="city")
	private String city;
	
	@NotNull(message ="is required")
	@Column(name="country")
	private String country;
	
	@NotNull(message = "is required")
	@Min(value = 1, message = "Must be age greater than or equals 1")
	@Max(value = 200, message = "Must be age less than or equals 200")
	@Column(name="age")
	private Integer age;
	
     
    public Customer() {
    	 
    }
    
	public Customer(String firstName, String lastName, String email, String image, String gender, String city,
			String country, Integer age, String phoneNo) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.image = image;
		this.gender = gender;
		this.city = city;
		this.country = country;
		this.age = age;
		this.phoneNo = phoneNo; 
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", image=" + image + ", gender=" + gender + ", city=" + city + ", country=" + country + ", age=" + age
				+ "]";
	}
}
