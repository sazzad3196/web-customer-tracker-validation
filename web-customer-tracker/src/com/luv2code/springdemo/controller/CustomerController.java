package com.luv2code.springdemo.controller;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.luv2code.springdemo.entity.Customer;
import com.luv2code.springdemo.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	 @Autowired
	 private CustomerService customerService;
	
	 @InitBinder
	 public void initBinder(WebDataBinder dataBind) {
		 StringTrimmerEditor editor = new StringTrimmerEditor(true);
		 dataBind.registerCustomEditor(String.class, editor);
	 }
	 
	 @GetMapping("/showFormForAdd")
	 public String customerForm(Model model) {
		 Customer theCustomer = new Customer();
		 model.addAttribute("customer", theCustomer);
		 return "customer-form";
	 }
	 
	 @PostMapping("/saveOrUpdateCustomer")
	 public String saveCustomer(@Valid @ModelAttribute("customer") Customer theCustomer, BindingResult theBind) {
		 if(theBind.hasErrors()) {
			 return "customer-form";
		 }
		 else {
			 customerService.saveOrUpdate(theCustomer);
			 return "redirect:/customer/list";
		 }
		 
	 }
	 
	 @GetMapping("/list")
	 public String findAllCustomers(Model model) {
		 List<Customer> customers = customerService.findAllCustomer();
		 model.addAttribute("customers", customers);
		 return "customer-list";
	 }
	 
	 @GetMapping("/delete")
	 public String deleteCustomer(@RequestParam("customerId") int id) {
		 if(customerService.deleteCustomer(id)) {
			 return "redirect:/customer/list";
		 }
		 
		 return "customer-list";
	 }
	 
	 @GetMapping("/showFormForUpdate")
	 public String updateCustomer(@RequestParam("customerId") int id, Model model) {
		  Customer customer = customerService.getCustomerById(id);
		  model.addAttribute("customer", customer);
		  return "customer-form";
	 }
	 
	 
     
}
