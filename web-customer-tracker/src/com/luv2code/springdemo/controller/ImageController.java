package com.luv2code.springdemo.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/image")
public class ImageController {
	
	@RequestMapping(value="/upload", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String uploadImage(@RequestPart("file") MultipartFile file, HttpSession session) {
		String path = session.getServletContext().getRealPath("/");
		String fileName = System.currentTimeMillis() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
		File f = new File(path + File.separator + "images");
		if(!f.exists()) {
			f.mkdirs();
		}
		
		File saveFile = new File(f.getAbsoluteFile() + File.separator + fileName);
		System.out.println("Image: " + saveFile.getAbsolutePath());
		try {
		    BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(saveFile));
		    byte[] b = file.getBytes();
		    bout.write(b);
		    bout.close();
		    return fileName;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return "File not store successfully.";
	}
	
	@PostMapping("/savefile")  
    public ModelAndView upload(@RequestParam CommonsMultipartFile file,HttpSession session){  
           String path=session.getServletContext().getRealPath("/");  
           String filename=file.getOriginalFilename(); 
           File f = new File(path + File.separator + "images");
           if(!f.exists()) {
           	f.mkdirs();
           }
            
           File saveFile = new File(f.getAbsoluteFile() + File.separator + filename);  
           System.out.println(saveFile.getAbsolutePath());  
           try{  
	            byte barr[]=file.getBytes();  
	              
	            BufferedOutputStream bout = new BufferedOutputStream(  
	                     new FileOutputStream(saveFile));  
	            bout.write(barr);
	            bout.flush();  
	            bout.close();  
             
           }catch(Exception e) {
           	System.out.println(e);
           }  
           return new ModelAndView("check ", "filename", filename );  
    }  
}
