package com.luv2code.springdemo.service;

import java.util.List;

import com.luv2code.springdemo.entity.Customer;

public interface CustomerService {
	 public List<Customer> findAllCustomer();
	 public boolean deleteCustomer(int id);
	 public boolean saveOrUpdate(Customer customer);
	 public Customer getCustomerById(int id);
}
