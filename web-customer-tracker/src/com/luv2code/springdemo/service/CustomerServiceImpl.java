package com.luv2code.springdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luv2code.springdemo.dao.CustomerDao;
import com.luv2code.springdemo.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerDao customerDao;
	
	@Override
	@Transactional
	public List<Customer> findAllCustomer() {
		return customerDao.findAllCustomer();
	}

	@Override
	@Transactional
	public boolean deleteCustomer(int id) {
		return customerDao.deleteCustomer(id);
	}

	@Override
	@Transactional
	public boolean saveOrUpdate(Customer customer) {
		return customerDao.saveOrUpdate(customer);
	}

	@Override
	@Transactional
	public Customer getCustomerById(int id) {
		return customerDao.getCustomerById(id);
	}

}
