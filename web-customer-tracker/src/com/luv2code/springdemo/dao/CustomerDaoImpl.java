package com.luv2code.springdemo.dao;

import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.luv2code.springdemo.entity.Customer;

@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Customer> findAllCustomer() {
		Session session = sessionFactory.getCurrentSession();
		Query<Customer> theQuery = session.createQuery("from Customer", Customer.class);
		List<Customer> theCustomers = theQuery.getResultList();
		return theCustomers;
	}

	@Override
	public boolean deleteCustomer(int id) {
		try {
			Session session = sessionFactory.getCurrentSession();
			Customer theCustomer = session.find(Customer.class, id);
			session.delete(theCustomer);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean saveOrUpdate(Customer customer) {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.saveOrUpdate(customer);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Customer getCustomerById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Customer customer = session.find(Customer.class, id);
		return customer;
	}

}
