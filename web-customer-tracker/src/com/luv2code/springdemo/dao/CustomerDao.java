package com.luv2code.springdemo.dao;

import java.util.List;

import com.luv2code.springdemo.entity.Customer;

public interface CustomerDao {
	 public List<Customer> findAllCustomer();
	 public boolean deleteCustomer(int id);
	 public boolean saveOrUpdate(Customer customer);
	 public Customer getCustomerById(int id);
}
