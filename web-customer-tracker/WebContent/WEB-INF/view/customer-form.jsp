<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Customer Form Page</title>
    <link type="text/css"
    	  rel="stylesheet"
    	  href="${pageContext.request.contextPath}/resources/css/style.css"> 
    <link type="text/css"
    	  rel="stylesheet"
    	  href="${pageContext.request.contextPath}/resources/css/add-customer-style.css"> 
    <link type="text/css"
    	  rel="stylesheet"
    	  href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"> 
    <link type="text/css"
    	  rel="stylesheet"
    	  href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css"> 
    	  
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>  
    
    <style type="text/css">
    	  .error {color: red}
    </style> 	  	 
     	   
</head>
<body>
	<div id="wrapper">
		 <div id="header">
	     	 <h2>Add Customer</h2>
	     </div> 
	</div><br><br>
	<div id="container">
	     <form:form action="saveOrUpdateCustomer" modelAttribute="customer" method="POST">
	            <form:hidden path="id"/>
	            
	            <table>
	                <tbody>
		                <tr>
			                <td><label>First name:</label></td>
				     		<td><form:input path="firstName"/></td>
				     		<td><form:errors path="firstName" cssClass="error"/>
			     		</tr>
			     		<tr>
			     			<td><label>Last name:</label></td>
				     		<td><form:input path="lastName"/></td>
				     		<td><form:errors path="lastName" cssClass="error"/>
			     		</tr>
			     		
			     		<tr>
			     			<td><label>Email:</label></td>
				     		<td><form:input path="email"/></td>
				     		<td><form:errors path="email" cssClass="error"/>
			     		</tr>
			     		
			     		<tr>
			     			<td><label>Phone number:</label></td>
				     		<td><form:input path="phoneNo"/></td>
				     		<td><form:errors path="phoneNo" cssClass="error"/>
			     		</tr>
			     		
			     		<tr>
			     			<td><label>Image:</label></td>
				     		<td><form:input path="image" id="image"/></td>
				     		<td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Upload Image</button></td>
				     		<td><form:errors path="image" cssClass="error"/>
			     		</tr>
			     		
			     		<tr>
			     		    <td><label></label></td>
			     			<td><img id="img" alt="alt" src="${pageContext.request.contextPath}/images/${customer.image}" height="200"></td>
			     		</tr>
			     		
			     		<tr>
			     			<td><label>City:</label></td>
				     		<td><form:select path="city">
				     		              <form:option value="" style="display: none;"></form:option>
				     					  <form:option value="dhaka" label="Dhaka"></form:option>
				     					  <form:option value="comilla" label="Comilla"></form:option>
				     					  <form:option value="barisal" label="Barisal"></form:option>
				     					  <form:option value="khulna" label="Khulna"></form:option>
				     					  <form:option value="kolkata" label="Kolkata"></form:option>
				     					  <form:option value="delli" label="Delli"></form:option>
				     				 </form:select> </td>
				     		<td><form:errors path="city" cssClass="error"/></td>
			     		</tr>	
			     		
			     		<tr>
			     			<td><label>Country:</label></td>		 
			     	        <td><form:select path="country">
			     	        		<form:option value="" style="display: none;"></form:option>
			     	    	  		<form:option value="bangladesh" label="Bangladesh"></form:option>
			     	    	  		<form:option value="india" label="India"></form:option>
			     	    	  		<form:option value="usa" label="USA"></form:option>
			     	    	    </form:select></td>
			     	    	<td><form:errors path="country" cssClass="error"/></td>  
			     	    </tr>
			     	    <tr>
			     			<td><label>Gender:</label></td>	  
			     	   		<td><form:select path="gender">
			     	   				<form:option value="" style="display: none;"></form:option>
			     	    	  		<form:option value="male" label="Male"></form:option>
			     	    	  		<form:option value="female" label="Female"></form:option>
			     	    	  		<form:option value="other" label="Other"></form:option>
			     	    	    </form:select></td>  
			     	    	<td><form:errors path="gender" cssClass="error"/></td>    
			     	    </tr>
			     	   
			     	   <tr>
			     			<td><label>Age:</label></td>	
			     			<td><form:input path="age"/></td>
			     			<td><form:errors path="age" cssClass="error"/>
			     	   <tr>
			     	   <tr>
			     			<td><label></label></td>	
			     	        <td><input type="submit" value="Submit"/></td>
			     	   </tr>     
		     	   </tbody>
	     	   </table>
	     </form:form>
	</div>
	
	<div class="modal fade" id="myModal" role="dialog">
		 	 <div class="modal-dialog">
			    <div class="modal-content">
			
			      <!-- Modal Header -->
			      <div class="modal-header">
			        <h4 class="modal-title">Image Uploading</h4>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			      </div>
			
			      <!-- Modal body -->
			      <div class="modal-body">
			        	<form action="${pageContext.request.contextPath}/image/upload" id="upload" method="post" enctype="multipart/form-data">
			        		 <div class="form-group">
			        		     <input type="file" class="form-control" name="file" placeholder="Select Image" />
			        		 </div>
			        		 <br>
			        		  <div class="form-group">
			        		       <button type="submit" class="btn btn-info btn-lg">Upload</button>
			        		  </div>
			        	</form>
			      </div>
			
			      <!-- Modal footer -->
			      <div class="modal-footer">
			      		
			      </div>
			
			  </div>
		</div>
    </div>
    
   <script>
   		$(function() {
   			$('#upload').ajaxForm({
   				success: function(msg) {
   					$('#image').val(msg);
   					$('#img').attr("src", "${pageContext.request.contextPath}/images/" + msg);
   					$('#myModal').modal('toggle');
   				},
   				error: function(msg) {
   					alert(msg);
   				}
   			});
   			
   		});
   </script>
</body>
</html>